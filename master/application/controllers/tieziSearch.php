<?php
/**
 * Created by PhpStorm.
 * User: mymacbook
 */
    $action = $_GET['action'];

    switch($action) {
        case 'init_data_list':
            init_data_list();
            break;
        case 'add_row':
            add_row();
            break;
        case 'del_row':
            del_row();
            break;
        case 'edit_row':
            edit_row();
            break;
    }

    //查询方法
    function init_data_list(){

        //查询表
        $sql = "SELECT * FROM `tiezi`";
        $query = query_sql($sql);
        while($row = $query->fetch_assoc()){
            $data[] = $row;
        }

        $json = json_encode(array(
            "resultCode"=>200,
            "message"=>"查询成功！",
            "data"=>$data
        ),JSON_UNESCAPED_UNICODE);

        //转换成字符串JSON
        echo($json);
    }

    //删除方法
    function del_row(){
        //测试
        /*echo "ok!";*/

        //接收传回的参数
        $rowId = $_GET['rowId'];
        $sql = "delete from tiezi where tiezi_id='$rowId'";

        if(query_sql($sql)){
            echo "ok!";
        }else{
            echo "删除失败！";
        }
    }

    //新增方法
    function add_row(){
        /*获取从客户端传过来的数据*/
        $tieziTitle = $_GET['tiezi_title'];
        $tieziAuthor = $_GET['tiezi_author'];
        $tieziTime = $_GET['tiezi_time'];
        $tieziClass = $_GET['tiezi_class'];
        $tieziContent = $_GET['tiezi_content'];
        $tieziImage = $_GET['tiezi_image'];
        $tieziId = $_GET['tiezi_id'];
        //INSERT INTO 表名 （列名1，列名2，...）VALUES （'对应的数据1'，'对应的数据2'，...）
        // VALUES 的值全为字符串，因为表属性设置为字符串
        $sql = "INSERT INTO t_users (tiezi_title,tiezi_author,tiezi_time,tiezi_class,tiezi_content,tiezi_image,tiezi_id) VALUES ('$tieziTitle','$tieziAuthor','$tieziTime','$tieziClass','$tieziContent','$tieziImage','$tieziId')";
        if(query_sql($sql)){
            echo "ok!";
        }else{
            echo "新增成功！";
        }
    }


    /**查询服务器中的数据
     * 1、连接数据库,参数分别为 服务器地址 / 用户名 / 密码 / 数据库名称
     * 2、返回一个包含参数列表的数组
     * 3、遍历$sqls这个数组，并把返回的值赋值给 $s
     * 4、执行一条mysql的查询语句
     * 5、关闭数据库
     * 6、返回执行后的数据
     */
    function query_sql(){
        $mysqli = new mysqli("127.0.0.1", "root", "18851191368", "jingyouluntan");
        $sqls = func_get_args();
        foreach($sqls as $s){
            $query = $mysqli->query($s);
        }
        $mysqli->close();
        return $query;
    }
?>