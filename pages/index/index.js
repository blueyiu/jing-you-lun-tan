//index.js
//获取应用实例


Page({
  data: {
    array: [{
      message: 'foo',
    }, {
      message: 'bar'
    }],

    types: ['topLeft', 'topRight', 'bottomLeft', 'bottomRight'],
    typeIndex: 3,
    colors: ['light', 'stable', 'positive', 'calm', 'balanced', 'energized', 'assertive', 'royal', 'dark'],
    colorIndex: 4,
    buttons: [{
      openType: 'getUserInfo',
      label: 'GetUserInfo',
      icon: '../../pages/img/shezhi.png'
    },
    {
      openType: 'share',
      label: '分享',
      icon: '../../pages/img/fenxiang.png'
    },
    {
      label: '发帖',
      icon: '../../pages/img/brush.png'
    },
    ],
    position: 'bottomRight'

  },

  onClick(e) {
    console.log('onClick', e.detail)
    if (e.detail.index == 2) {
      wx.navigateTo({
        url: '../postMessage/postMessage'
      })
    }
  },
  onContact(e) {
    console.log('onContact', e)
  },
  onGotUserInfo(e) {
    console.log('onGotUserInfo', e)
  },
  onGotPhoneNumber(e) {
    console.log('onGotPhoneNumber', e)
  },
  onChange(e) {
    console.log('onChange', e)
  },
  pickerChange1(e) {
    const typeIndex = e.detail.value
    const position = this.data.types[typeIndex]

    this.setData({
      typeIndex,
      position,
    })
  },
  pickerChange2(e) {
    const colorIndex = e.detail.value
    const theme = this.data.colors[colorIndex]

    this.setData({
      colorIndex,
      theme,
    })
  },

  searchClick(e){
    wx.navigateTo({
      url: '../search/search',
    })
  },

  onLoad: function (option) {

  }

})
